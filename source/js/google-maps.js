(function() {
  'use strict';

  var map;     // will be an instance of a Google Map
  var service; // will be an instance of Google Maps' Places library
  var bounds;  // will be an instance of Google Maps' Lat/Lng bounds
  var officeSelectMenus = document.querySelectorAll('.erg-office-select');
  var officeListItems   = document.querySelectorAll('.erg-office-list li');
  var officeLocations   = []; // will be an array of objects containing Google maps info for each office location



  /**
   * Setup officeLocations array with Google Place ID of each office
   */
  function setupOfficeLocations() {
    var office;
    // iterate over office list items NodeList
    for (var i = 0; i < officeListItems.length; ++i) {
      // enteredText is the trimmed, entered text
      var enteredText = officeListItems[i].textContent.trim();
      // nameParts is an array of the entered text, split on opening parenthesis
      var nameParts = enteredText.split('(');
      // all offices will have an officeName
      var officeName = nameParts[0];
      // some offices will have a subname, indicated by an open parenthesis
      var officeSubname = nameParts[1];
      // if an office has subname, remove the trailing closing parenthesis
      if (officeSubname) { officeSubname = officeSubname.slice(0, -1); }

      // build officeLocations array with objects containing Google maps info
      office = {
        id:      enteredText,
        name:    officeName,
        subname: officeSubname, // will be undefined for offices without a subname
        placeId: officeListItems[i].getAttribute('data-place-id'),
        infoWindow:     null, // will be a Google maps info window
        locationMarker: null  // will be a Google Maps location marker
      };
      officeLocations.push(office);
      // attach a click event handler to each office list item, selecting the office location
      officeListItems[i].addEventListener('click', selectOffice, false);
    }
    // iterate over office select menu NodeList
    for (var i = 0; i < officeSelectMenus.length; ++i) {
      // attach a change event handler to each office select menu, selecting the office location
      officeSelectMenus[i].addEventListener('change', selectOffice, false);
    }
  }



  /**
   * Focuses Google Map on the clicked or selected office location
   */
  function selectOffice(event) {
    var targetOffice;
    // set targetOffice to the selected option
    if (event.type === 'change') { targetOffice = event.target.options[event.target.selectedIndex]; }
    // set targetOffice to the clicked list item
    if (event.type === 'click') { targetOffice = event.target; }
    // set targetOfficeId from selected/clicked targetOffice DOM node
    var targetOfficeId = targetOffice.textContent.trim();

    // iterate over the officeLocations array
    for (var i = 0; i < officeLocations.length; ++i) {
      // pass loop index to closure
      (function(index) {

        var office = officeLocations[index];

        // filter through officeLocations to get the target office
        if (office.id === targetOfficeId) {
          // Google Maps's Place library's getDetails() method takes two arguments:
          // 1. request object (containing placeId)
          // 2. callback function (containing PlaceResult object and status code)
          service.getDetails({ placeId: office.placeId }, function(place, status) {
            // call focusLocationMarker() for the target office location
            focusLocationMarker(place, status, office);
          });
        }

      })(i);
    }
  }



  /**
   * Callback function for Google Maps' Places library's getDetails() method
   */
  function focusLocationMarker(place, status, office) {
    // ensure Google Place ID exists
    if (status !== google.maps.places.PlacesServiceStatus.OK) { return }

    // center and zoom map
    map.setCenter(place.geometry.location);
    map.setZoom(18);

    // trigger a Google Maps click event on the location marker, opening the info window
    google.maps.event.trigger(office.locationMarker, 'click' );
  }



  /**
   * Resets the Google Map to the original size and zoom
   * @constructor
   * @param {element}            container
   * @param {google.maps.Map}    map
   * @param {google.maps.LatLng} mapOptions
   */
  function ResetMapButton(container, map, mapOptions) {
    var button = document.createElement('div');
    button.className = 'erg-map-button';
    button.innerHTML = 'Reset Map';
    // append button to container element
    container.appendChild(button);
    // attach a click handler to the button,
    // setting zoom, re-centering map, and closing all info windows
    button.addEventListener('click', function() {
      // re-center and zoom map to show all the location icons
      map.fitBounds(bounds);

      // close all info windows
      for (var i = 0; i < officeLocations.length; ++i) {
        officeLocations[i].infoWindow.close();
      }
    });
  }



  /**
   * Initializes a Google Map instance
   */
  function initMap() {
    var mapContainer = document.getElementById('google-map');
    // initially center and zoom map to show the full United States
    var mapOptions = {
      center: {
        lat: 37.090,
        lng: -95.713
      },
      zoom: 3
    };

    // instantiate new map, places service, and lat/long bounds instances
    map     = new google.maps.Map(mapContainer, mapOptions);
    service = new google.maps.places.PlacesService(map);
    bounds  = new google.maps.LatLngBounds();

    // iterate over the officeLocations array
    for (var i = 0; i < officeLocations.length; ++i) {
      // pass loop index to closure
      (function(index) {

        var office = officeLocations[index];

        // create a new Google Maps info window for each office location
        office.infoWindow = new google.maps.InfoWindow();

        // Google Maps Query Limit is 10 requests at a time...
        // ...so callGetDetails() throttles call to Google Maps PlacesService
        function callGetDetails() {
          // Google Maps's Place library's getDetails() method takes two arguments:
          // 1. request object (containing placeId)
          // 2. callback function (containing PlaceResult object and status code)
          service.getDetails({ placeId: office.placeId }, function(place, status) {
            // call setupLocationMarker() for each office location
            setupLocationMarker(place, status, office);
          });
        }

        // if index is not devisible by 10, recursively call function
        if (index % 10 !== 0) {
          callGetDetails();
        // after every 10th index, delay 2 seconds before resuming
        } else {
          window.setTimeout(function() {
            //console.log(index);
            callGetDetails();
          }, 2000);
        }

      })(i);
    }

    // create a new reset map button from the ResetMapButton() constructor
    var buttonContainer = document.createElement('div');
    var resetButton = new ResetMapButton(buttonContainer, map, mapOptions);
    // position the buttonContainer div at the top of the map
    buttonContainer.index = 1;
    buttonContainer.style.paddingTop = '10px';
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(buttonContainer);
  }



  /**
   * Callback function for Google Maps' Places library's getDetails() method
   */
  function setupLocationMarker(place, status, office) {
    // ensure Google Place ID exists
    if (status !== google.maps.places.PlacesServiceStatus.OK) {
      console.error(status);
      return;
    }

    // create a new Google Maps location marker for each office location
    office.locationMarker = new google.maps.Marker({
      map: map,
      position: place.geometry.location,
    });

    // extend the map's bounds to include the position of the office location
    bounds.extend(office.locationMarker.getPosition());
    map.fitBounds(bounds);

    // create a Google Maps info window for each office location
    createInfoWindow(place, office);

    // attach a click handler to each Google Maps location marker
    openMarkerOnClick(office);
  }



  /**
   * Creates a Google Maps info window for each office location
   */
  function createInfoWindow(place, office) {
    // info object will contain all the info for the Google Maps infoWindow
    var info = {};

    // set name from office object
    info.name = office.name;
    // conditionally add office subname if it exists
    if (office.subname) { info.name += '<br>(' + office.subname + ')'; }

    // addr_parts object will contain the address components for each office
    var addr_parts = {};
    // iterate over place.address_components array
    for (var i = 0; i < place.address_components.length; ++i) {
      var components = place.address_components[i];
      var type = components.types[0];
      var text = components.short_name;
      // build up addr_parts object with component type and short name
      addr_parts[type] = text;
    }

    // first address line is street number and street
    info.addr_line1 = addr_parts.street_number + ' ' + addr_parts.route;
    // conditionally add subpremise if it exists
    if (addr_parts.subpremise) { info.addr_line1 += ' ' + addr_parts.subpremise; }
    // second address line is city, state, and zip code
    info.addr_line2 = addr_parts.locality + ', ' + addr_parts.administrative_area_level_1 + ' ' + addr_parts.postal_code;

    // phone number and Google Maps url already formatted in place object
    info.phone = place.formatted_phone_number;
    info.url   = place.url;

    // define Google Maps info window content
    office.infoWindow.setContent(
      '<div class="erg-office-info">' +
        '<p class="erg-office-name">' + info.name + '</p>' +
        '<p class="erg-office-address">' + info.addr_line1 + '<br>' + info.addr_line2 + '</p>' +
        '<p class="erg-office-phone"><a href="tel:' + info.phone + '">' + info.phone + '</a></p>' +
        '<p class="erg-office-google"><a href="' + info.url + '">View on Google Maps</a></p>' +
      '</div>'
    );
  }



  /**
   * Google Maps click handler for when a Google Maps location marker is clicked
   */
  function openMarkerOnClick(office) {
    google.maps.event.addListener(office.locationMarker, 'click', function() {
      // center and zoom map
      map.setCenter(this.getPosition());
      map.setZoom(18);

      // open info window
      office.infoWindow.open(map, office.locationMarker);
    });
  }



  // if officeSelectMenus exist on page, kick things off
  if (officeSelectMenus.length > 0) {
    setupOfficeLocations();
    initMap();
  }

}());
