var jQuery = require('jquery');

(function($) {
  'use strict';

  // NodeList of local links (each starts with '#')
  var localLinks = document.querySelectorAll('a[href^="#"]');

  // main navigation contact link (defined for both static and served versions)
  var contactLink =
    document.querySelector('a[href="about.html#contact"]') ||
    document.querySelector('a[href="/about#contact"]');

  // toggle for showing/hiding site navigation (only visible on small screens)
  var navToggle = document.querySelector('.erg-nav-toggle');

  // array will contain all the links to smooth scroll
  var scrollLinks = [];

  // height of the site header when body is scrolled
  var headerHeight = 68;

  // array will contain links to exclude from smooth scrolling
  var excludeList = [];



  function setupScrollLinksArray() {
    // push the localLinks NodeList items into the scrollLinks array
    for (var i = 0; i < localLinks.length; ++i) {
      scrollLinks.push(localLinks[i]);
    }
    // push the contact link into the scrollLinks array
    scrollLinks.push(contactLink);
  }



  function setupExcludeList() {
    // add slider tabs' anchors to exclude list
    var nodeList = document.querySelectorAll('[data-feature="slider-tabs"] a');
    if (nodeList) {
      for (var i = 0; i < nodeList.length; i++) {
        excludeList.push(nodeList[i]);
      }
    }
    // add navToggle to exclude list
    excludeList.push(navToggle);
  }



  function getDistanceFromPageTop(element) {
    var distance = 0;
    if (element.offsetParent) {
      do {
        distance += element.offsetTop;
        element = element.offsetParent;
      } while (element);
    }
    return distance >= 0 ? distance : 0;
  }



  function scrollPageIfHashExists() {
    if (window.location.hash) { smoothScroll(); }
  }



  function smoothScroll(event) {
    var offset = headerHeight - 1; // site header height - header bottom border
    var anchorID;                  // set from the target or hash of the element
    var element;                   // set from the anchor ID
    var marginTop;                 // the top margin of the element
    var scrollDistance;            // distance to scroll, accounting for offset

    // if there's no event, function was called because page loaded w/ a hash
    if (!event) {
      anchorID = window.location.hash;
    // else, a click event fired
    } else {
      // return early if element is within the exclude list
      for (var i = 0; i < excludeList.length; ++i) {
        if (excludeList[i].contains(event.target)) { return; }
      }
      // if clicked anchor's target is on the same page, prevent jump to anchor
      if (event.target.href.split('#')[0] === window.location.href) {
        event.preventDefault();
      }
      // if a hash exists, use it; else set to '#main-content' (top of page)
      anchorID = event.target.hash ? event.target.hash : '#main-content';
    }

    // set anchor element from anchorID
    element = document.querySelector(anchorID);

    // compute top margin of anchor element to add to offset
    marginTop = parseInt(window.getComputedStyle(element).marginTop, 10);
    // if there's a top margin, add it to offset
    if (marginTop) { offset += marginTop; }

    // set scroll distance, accounting for offset
    scrollDistance = getDistanceFromPageTop(element) - offset;

    // correct faulty scroll position for 'Contact Us' link...
    // ...page will have scrolled changing header's getDistanceFromTop() value
    if (anchorID === '#contact') { scrollDistance -= (headerHeight + 1) }

    // smoothscroll to anchor element
    $('html, body').animate({ scrollTop: scrollDistance }, 'slow');
  }



  setupScrollLinksArray();
  setupExcludeList();
  scrollPageIfHashExists();

  // smooth scroll all scrollLinks
  for (var i = 0; i < scrollLinks.length; ++i) {
    scrollLinks[i].addEventListener('click', smoothScroll, false);
  }

}(jQuery));
