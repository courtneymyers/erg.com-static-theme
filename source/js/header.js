(function() {
  'use strict';

  var siteHeader   = document.querySelector('.erg-site-header');
  var headerHeight = siteHeader.getBoundingClientRect().height;
  var navToggle    = document.querySelector('.erg-nav-toggle');
  var toggleState; // will be set via setToggleState()

  function toggleNavMenu(event) {
    // return early if navToggle isn't visible (large screens)
    if (window.getComputedStyle(navToggle).getPropertyValue('display') === 'none') { return }
    // return early if the clicked element isn't in the site header
    if (!siteHeader.contains(event.target)) { return }
    // return early if the clicked element isn't an anchor
    if (event.target.nodeName !== 'A') { return }

    // if the navToggle is active
    if (navToggle.getAttribute('data-active') === 'true') {
      // close the navToggle
      navToggle.setAttribute('data-active', 'false');
    // else, the navToggle isn't active, so make it active
    } else {
      navToggle.setAttribute('data-active', 'true');
    }
  }

  function transitionHeader(event) {
    if ( window.pageYOffset > headerHeight / 2 ) {
      document.body.setAttribute('data-scrolled', 'true');
    }
    if ( window.pageYOffset < headerHeight / 2 ) {
      document.body.setAttribute('data-scrolled', 'false');
    }
  }

  document.addEventListener('scroll', transitionHeader, false);

  document.addEventListener('click', toggleNavMenu, false);

}());
