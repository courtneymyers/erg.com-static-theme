(function() {

  // get all sliders on the page
  const foundSliders = document.querySelectorAll('[data-feature="slider"]');

  // if there aren't any sliders on the page, return early
  if (!foundSliders) { return; }

  // allSliders array will contain data for each slider
  const allSliders = [];

  (function allSlidersSetup1() {
    // build up allSliders array from foundSliders NodeList:
    // give each slider a unique number, element containing the slider element,
    // parts object for the child elements, and nav object for tracking panels
    for (let i = 0; i < foundSliders.length; ++i) {
      allSliders[i] = {
        number  : i + 1,
        element : foundSliders[i],
        parts   : {},
        nav     : {},
      };
    }
  })();

  (function allSlidersSetup2() {
    // iterate over all allSliders array, setting up vaiables in the parts and nav objects
    for (let i = 0; i < allSliders.length; ++i) {
      const slider = allSliders[i];
      // child elements inside each slider
      slider.parts.tabs    = slider.element.querySelector('[data-feature="slider-tabs"]');
      slider.parts.dots    = slider.element.querySelector('[data-feature="slider-dots"]');
      slider.parts.arrows  = slider.element.querySelector('[data-feature="slider-arrows"]');
      slider.parts.panels  = slider.element.querySelector('[data-feature="slider-panels"]');
      slider.parts.numbers = slider.element.querySelector('[data-feature="slider-numbers"]');
      slider.parts.menu    = slider.element.querySelector('[data-feature="slider-menu"]');
      // conditionally set prev and next arrows
      if (slider.parts.arrows) {
        slider.parts.arrowPrev = slider.parts.arrows.querySelector('[data-arrow="prev"] a');
        slider.parts.arrowNext = slider.parts.arrows.querySelector('[data-arrow="next"] a');
      }
      // total number of panels
      slider.nav.total = slider.parts.panels.childElementCount;
      // current, next, and prev will be set initially, and reset after each panel change
      slider.nav.current;
      slider.nav.next;
      slider.nav.prev;
    }
  })();



  function callAtBreakpoint(type, length, callback) {
    const breakpoint = `(${type}-width: ${length})`;
    window.matchMedia(breakpoint).addListener(function(event) {
      if (event.matches) { callback(); }
    });
  }



  function toggleAttribute(element, attribute, value1 = 'true', value2 = 'false') {
    element.setAttribute('data-' + attribute, element.getAttribute('data-' + attribute) === value1 ? value2 : value1);
  }



  function deselectElements(elements) {
    for (let i = 0; i < elements.length; ++i) {
      elements[i].setAttribute('data-active', 'false');
    }
  }



  function setupArrows(slider) {
    // incriment next when less than or equal to total, else set to one
    slider.nav.next = (slider.nav.current + 1) <= slider.nav.total ? (slider.nav.current + 1) : 1;
    // decrement prev when greater than zero, else set to total
    slider.nav.prev = (slider.nav.current - 1) > 0 ? (slider.nav.current - 1) : slider.nav.total;
    // get cardstackNumber from slider element's ID
    const cardstackNumber = slider.element.id.split('stack')[1];
    // setup arrow navigation
    slider.parts.arrowPrev.parentNode.setAttribute('data-item', slider.nav.prev);
    slider.parts.arrowNext.parentNode.setAttribute('data-item', slider.nav.next);
    slider.parts.arrowPrev.href = '#stack' + cardstackNumber + '-card' + slider.nav.prev;
    slider.parts.arrowNext.href = '#stack' + cardstackNumber + '-card' + slider.nav.next;
  }



  function setupSliders(slider) {
    const allPanels = slider.parts.panels.querySelectorAll('[data-feature="slider-panel"]');

    // compute each panel's width and all panels' widths
    const eachPanelWidth = 100 / allPanels.length;
    const allPanelsWidth = 100 * allPanels.length;

    // --- set allPanels width
    slider.parts.panels.style.width = allPanelsWidth + '%';
    // --- set each panel's positioning and width
    for (let i = 0; i < allPanels.length; ++i) {
      const eachPanel = allPanels[i];
      eachPanel.style.position = 'absolute';
      eachPanel.style.left     = i * eachPanelWidth + '%';
      eachPanel.style.width    = eachPanelWidth + '%';
    }

    // --- set first panel active
    const firstPanel = slider.parts.panels.children[0];
    firstPanel.setAttribute('data-active', 'true');

    // --- set current slide number to number from first slide's ID
    const firstSlideNumber = Number(slider.parts.panels.children[0].id.split('card-')[1]);
    // throw error and return early if first slide ID doesn't end in 1
    if (firstSlideNumber !== 1) {
      console.error('Markup error: First slide\'s ID must end in 1.')
      return;
    }
    slider.nav.current = firstSlideNumber;

    // --- conditionally setup tabs
    if (slider.parts.tabs) {
      const allTabs = slider.parts.tabs.querySelectorAll('[data-item]');
      // throw error and return early if number of tabs !== number of panels
      if (allTabs.length !== allPanels.length) {
        console.error('Markup error: Number of tabs must equal number of panels.');
        return;
      }
      // set each tab's width to match each panel's width
      function setTabWidth() {
        for (let i = 0; i < allTabs.length; ++i) {
          allTabs[i].style.width = eachPanelWidth + '%';
        }
      }
      // unset each tab's width
      function unsetTabWidth() {
        for (let i = 0; i < allTabs.length; ++i) {
          allTabs[i].style.width = '';
        }
      }
      // if the page is over 50em breakpoint, set tab width
      if (window.matchMedia('(min-width: 50em)').matches) {
        setTabWidth();
      }
      // set tab width at min-width breakpoint, and undo setting at max-width breakpoint
      callAtBreakpoint('min', '50em', setTabWidth);
      callAtBreakpoint('max', '50em', unsetTabWidth);
      // set first tab active
      const firstTab = slider.parts.tabs.children[0].children[0];
      firstTab.setAttribute('data-active', 'true');
    }

    // --- conditionally setup menu
    if (slider.parts.menu) {
      const firstMenuItem = slider.parts.menu.children[1].children[0];
      firstMenuItem.setAttribute('data-active', 'true');
    }

    // --- conditionally setup total slides text
    if (slider.parts.numbers) {
      const totalSlidesText = slider.parts.numbers.querySelector('[data-feature="slider-number-total"]');
      totalSlidesText.textContent = slider.nav.total;
    }

    // --- conditionally setup dots
    if (slider.parts.dots) {
      const allDots = slider.parts.dots.querySelectorAll('[data-item]');
      // throw error and return early if number of dots !== number of panels
      if (allDots.length !== allPanels.length) {
        console.error('Markup error: Number of dots must equal number of panels.');
        return;
      }
      // set first dot active
      const firstDot = slider.parts.dots.children[0].children[0];
      firstDot.setAttribute('data-active', 'true');
    }

    // --- conditionally setup arrows
    if (slider.parts.arrows) {
      setupArrows(slider);
    }
  }



  function cyclePanels(event) {
    // get panels from clicked slider
    const panels = this.querySelector('[data-feature="slider-panels"]');

    // return early if clicked element is inside a panel
    if (panels.contains(event.target)) { return; }
    // return early if clicked element isn't an anchor
    if (event.target.nodeName !== 'A') { return; }
    // prevent navigation
    event.preventDefault();

    // if the slider menu toggle is clicked, toggle the menu
    if (event.target.getAttribute('data-feature') === 'slider-menu-toggle') {
      toggleAttribute(event.target, 'active');
      return;
    }

    // get slider from allSliders array (could be multiple sliders on page)
    let slider;
    // iterate over allSliders array
    for (let i = 0; i < allSliders.length; ++i) {
      // use this (clicked element) to assign slider from allSliders array
      if (this === allSliders[i].element) {
        slider = allSliders[i];
      }
    }

    // reset current slide number to item number from clicked element
    const itemNumber = event.target.parentNode.getAttribute('data-item');
    slider.nav.current = Number(itemNumber);

    // deselect tabs, dots, or menu items if they exist
    if (slider.parts.tabs) {
      const allTabs = slider.parts.tabs.children[0].children;
      deselectElements(allTabs);
    }
    if (slider.parts.dots) {
      const allDots = slider.parts.dots.children[0].children;
      deselectElements(allDots);
    }
    if (slider.parts.menu) {
      const allMenuItems = slider.parts.menu.children[1].children;
      deselectElements(allMenuItems);
    }

    // get collection of elements with current slide number (tab, dot, menu item, and/or arrow)
    const currentElements = slider.element.querySelectorAll('[data-item="' + slider.nav.current + '"]');
    // make current elements active (could be only one item or multiple items)
    for (let i = 0; i < currentElements.length; ++i) {
      // filter out arrows and set data-active to true for remaining elements
      if (!currentElements[i].hasAttribute('data-arrow')) {
        currentElements[i].setAttribute('data-active', 'true');
      }
    }

    // deselect all panels and select current panel
    const allPanels    = slider.parts.panels.children;
    const currentPanel = slider.parts.panels.querySelector(event.target.hash);
    deselectElements(allPanels);
    currentPanel.setAttribute('data-active', 'true');

    // shift all panels specified percent
    const distance = 100 - (100 * itemNumber);
    slider.parts.panels.style.left = distance + '%';

    // if slider menu exists, close it
    if (slider.parts.menu) {
      const sliderMenu = slider.parts.menu.children[0];
      sliderMenu.setAttribute('data-active', 'false');
    }

    // if slide numbers exist, set current slide number's text
    if (slider.parts.numbers) {
      const currentSlideText = slider.parts.numbers.querySelector('[data-feature="slider-number-current"]');
      currentSlideText.textContent = slider.nav.current;
    }

    // if slide arrows exist, reset arrow navigation
    if (slider.parts.arrows) {
      setupArrows(slider);
    }
  }



  (function initialize() {
    // iterate over allSliders, setting up each slider and attaching an event listener
    for (let i = 0; i < allSliders.length; ++i) {
      const slider = allSliders[i];
      setupSliders(slider);
      slider.element.addEventListener('click', cyclePanels, false);
    }
  })();

}());
